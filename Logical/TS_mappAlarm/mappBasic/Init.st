

PROGRAM _INIT
		
	//gAxis1
	//MpAxis1AlarmUIConnect is MpAlarmBasicUIConnect20Type because there is only 14 MpAxis alarms and one user alarm
	MpAlarmBasicUISetupAxis1.AcknowledgeImage := ADR(MpAxis1AlarmUIConnect.AcknowledgeImage);
	MpAlarmBasicUISetupAxis1.AddInfo := ADR(MpAxis1AlarmUIConnect.AddInfo);
	MpAlarmBasicUISetupAxis1.AddInfoStringSize := SIZEOF(MpAxis1AlarmUIConnect.AddInfo[0]);
	MpAlarmBasicUISetupAxis1.AlarmImage := ADR(MpAxis1AlarmUIConnect.AlarmImage);
	MpAlarmBasicUISetupAxis1.ImageSize := SIZEOF(MpAxis1AlarmUIConnect.AlarmImage);
	
	//MpAlarmBasicUI FB init settings
	MpAlarmBasicUI_Axis1.MpLink := ADR(gAxis1);	
	MpAlarmBasicUI_Axis1.Mode := mpALARM_BASIC_UI_MODE_ALL;
	MpAlarmBasicUI_Axis1.UISetup := MpAlarmBasicUISetupAxis1;
	
	//gAxis2
	//MpAxis2AlarmUIConnect is MpAlarmBasicUIConnect20Type because there is only 14 MpAxis alarms and one user alarm
	MpAlarmBasicUISetupAxis2.AcknowledgeImage := ADR(MpAxis2AlarmUIConnect.AcknowledgeImage);
	MpAlarmBasicUISetupAxis2.AddInfo := ADR(MpAxis2AlarmUIConnect.AddInfo);
	MpAlarmBasicUISetupAxis2.AddInfoStringSize := SIZEOF(MpAxis2AlarmUIConnect.AddInfo[0]);
	MpAlarmBasicUISetupAxis2.AlarmImage := ADR(MpAxis2AlarmUIConnect.AlarmImage);
	MpAlarmBasicUISetupAxis2.ImageSize := SIZEOF(MpAxis2AlarmUIConnect.AlarmImage);
	
	//MpAlarmBasicUI FB init settings
	MpAlarmBasicUI_Axis2.MpLink := ADR(gAxis2);
	MpAlarmBasicUI_Axis2.Mode := mpALARM_BASIC_UI_MODE_ALL;
	MpAlarmBasicUI_Axis2.UISetup := MpAlarmBasicUISetupAxis2;
	
	//gMain
	//MpAxis2AlarmUIConnect is MpAlarmBasicUIConnect200Type (user defined) because there is 200 user alarms
	MpAlarmBasicUISetupMain.AcknowledgeImage := ADR(MainAlarmUIConnect.AcknowledgeImage);
	MpAlarmBasicUISetupMain.AddInfo := ADR(MainAlarmUIConnect.AddInfo);
	MpAlarmBasicUISetupMain.AddInfoStringSize := SIZEOF(MainAlarmUIConnect.AddInfo[0]);
	MpAlarmBasicUISetupMain.AlarmImage := ADR(MainAlarmUIConnect.AlarmImage);
	MpAlarmBasicUISetupMain.ImageSize := SIZEOF(MainAlarmUIConnect.AlarmImage);
	
	//MpAlarmBasicUI FB init settings
	MpAlarmBasicUI_Main.MpLink := ADR(gMain);
	MpAlarmBasicUI_Main.Mode := mpALARM_BASIC_UI_MODE_USER;
	MpAlarmBasicUI_Main.UISetup := MpAlarmBasicUISetupMain;
	
	
	//mapp FBs basic init settings
	MpAxisBasic_Axis1.MpLink := ADR(gAxis1);
	MpAxisBasic_Axis1.Axis := ADR(gAxis01);
	MpAxisBasic_Axis1.Parameters := ADR(Axis01Par);
	MpAxisBasic_Axis1.Enable := 1;
	
	MpAxisBasic_Axis2.MpLink := ADR(gAxis2);
	MpAxisBasic_Axis2.Axis := ADR(gAxis02);	
	MpAxisBasic_Axis2.Parameters := ADR(Axis02Par);
	MpAxisBasic_Axis2.Enable := 1;
	
END_PROGRAM