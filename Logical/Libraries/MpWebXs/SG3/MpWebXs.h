/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _MPWEBXS_
#define _MPWEBXS_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG4
		#include "AsArSdm.h"
		#include "MpCom.h"
#endif

#ifdef _SG3
		#include "AsArSdm.h"
		#include "MpCom.h"
#endif

#ifdef _SGC
		#include "AsArSdm.h"
		#include "MpCom.h"
#endif




#ifdef __cplusplus
};
#endif
#endif /* _MPWEBXS_ */

